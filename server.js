//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.use(express.static(__dirname + '/build/default'))

var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

  app.get('/', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  res.sendFile("index.html", {root: '.'});
});
