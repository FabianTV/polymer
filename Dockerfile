#Imagen base
FROM node:latest

#Directorio de la aplicacion en el contenedor
WORKDIR /app

#copiado de archivos
ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#comando
cmd ["npm", "start"]
